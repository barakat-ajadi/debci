require 'spec_helper'
require 'debci/extra_apt_source'

describe Debci::ExtraAptSource do
  let(:user1) do
    Debci::User.create!(uid: 'foo1@bar.com'.hash % 1000, username: 'foo1@bar.com')
  end
  let(:user2) do
    Debci::User.create!(uid: 'foo2@bar.com'.hash % 1000, username: 'foo2@bar.com')
  end

  let(:entry) { "deb https://fasttrack.debian.net/debian-fasttrack/ bullseye-backports-staging main contrib" }
  let(:extra_apt_source_without_allowed_users) { Debci::ExtraAptSource.new("deb https://fasttrack.debian.net/debian-fasttrack/ bullseye-fasttrack main contrib") }
  let(:extra_apt_source) { Debci::ExtraAptSource.new(entry, [user1.id, 123]) }

  it 'sets allowed_users to [] if not present' do
    expect(extra_apt_source_without_allowed_users.instance_variable_get(:@allowed_users)).to eq([])
  end

  describe '.allowed?' do
    context 'when called for extra-apt-source without any allowed_users specified' do
      it 'returns true for all users' do
        expect(extra_apt_source_without_allowed_users.allowed?(user1)).to eq(true)
        expect(extra_apt_source_without_allowed_users.allowed?(user2)).to eq(true)
      end
    end

    context 'when called for extra-apt-source with any allowed_users' do
      it 'returns true when passed with allowed user' do
        expect(extra_apt_source.allowed?(user1)).to eq(true)
      end

      it 'returns false when passed with allowed user' do
        expect(extra_apt_source.allowed?(user2)).to eq(false)
      end
    end
  end
end
